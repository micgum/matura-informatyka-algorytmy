#include <iostream>

bool searchForPattern(std::string word, std::string pattern)
{
    if (word.size() < pattern.size()) {
        return false;
    }
    if (word == pattern) {
        return true;
    }

    bool result = false;

    for (size_t i = 0; i < word.size()-pattern.size(); i++) {
        result = true;
        for(size_t j = 0; j < pattern.size(); j++) {
            if (word[i+j] != pattern[j]) {
                result = false;
                break;
            }
        }

        if (result == true) {
            return true;
        }
    }
    return result;
}

int main(void)
{
    std::string word = "lokomotywa";
    std::string pattern_1 = "motyw";
    std::string pattern_2 = "kot";

    std::cout << searchForPattern(word, pattern_1) << "\n";
    std::cout << searchForPattern(word, pattern_2);

    return 0;
}