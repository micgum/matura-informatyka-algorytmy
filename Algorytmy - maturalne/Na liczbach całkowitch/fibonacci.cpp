#include <iostream>

long long fibonacci(long n)
{ // iterative
    long long temp_1 = 0, temp_2 = 1;

    for (int i = 1; i < n; i++) {
        temp_2 += temp_1;
        temp_1 = temp_2 - temp_1;
    }
    return temp_2;
}

long long fibonacci_not_optimalized(long n)
{ // recurrent
    if (n < 3) {
        return 1;
    }
    return fibonacci_not_optimalized(n - 2) + fibonacci_not_optimalized(n - 1);
}

int main(void)
{
    std::cout << fibonacci(10) << "\n";
    std::cout <<fibonacci_not_optimalized(10);

    return 0;
}