#include <iostream>
#include <algorithm> 
#include <vector>

/*
BUCKET SORT (for floating point values):
    Complexity:
        Worst-case:
            O(n^2)
        Best-case:
            O(n+k); k is komplexity of sorting individual buckets
        Average performance:
            O(n)
*/ 

void bucketSort(double arr[], int arr_size) 
{ // in increasing order 
    // creating arr_size empty buckets 
    std::vector<std::vector<double>> buckets(arr_size);

    // putting array elements into different buckets
    for (int i = 0; i < arr_size; i++) {
        int bucket_index = arr_size * arr[i];
        buckets[bucket_index].push_back(arr[i]);
    }

    // sorting every bucket
    for (int i = 0; i < arr_size; i++) {
        if (buckets[i].size() > 1) {
            std::sort(buckets[i].begin(), buckets[i].end());
        }
    }

    // concatenetion of all buckets into arr[]
    int index = 0;
    for (int i = 0; i < arr_size; i++) {
        for (int j = 0; j < buckets[i].size(); j++) {
            arr[index++] = buckets[i][j];
        }
    }
}

int main(void)
{
    double arr[] {.132, .1323, 13, .134, .1, .34, .31, .1, .33, .3345, .642, .1, .4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    bucketSort(arr, arr_size);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << ", ";
    }

    return 0;
}