def decToBase(number, base): # max base is 36
    result = ""
    while number > 0:
        digit = int(number % base)
        if digit < 10:
            result += str(digit)
        else:
            result += chr(ord('A') + digit - 10)
        number //= base
    result = result[::-1]
    return result

number = decToBase(10, 2)
print(number)