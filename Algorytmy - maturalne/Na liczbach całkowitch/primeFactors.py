from math import sqrt

def primeFactors(number):
    prime_num = int(sqrt(number))
    i = 2
    result = ""

    while (number > 1) and (i <= prime_num):
        while (number % i == 0):
            print(i, ' ', end='')
            number //= i
        i += 1
    
    if number > 1:
        print(number)


primeFactors(15)