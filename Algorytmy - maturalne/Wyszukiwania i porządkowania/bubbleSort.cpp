#include <iostream>

void swap(int *num_1, int *num_2) {
    int temp = *num_1;
    *num_1 = *num_2;
    *num_2 = temp;
}

/*
BUBBLE SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions
            O(n^2) swaps 
        Best-case:
            O(n) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n^2) swaps
*/  

void bubbleSort(int arr[], int arr_size) 
{ // in increasing order 
    for (int i = 0; i < arr_size-1; i++) {
        for (int j = 0; j < arr_size-i-1; j++) {
            if (arr[j] > arr[j+1]) { // for decreasing order use '<' operator 
                swap(&arr[j], &arr[j+1]);
            }
        }
    }
}

void bubbleSort_optimalized(int arr[], int arr_size) 
{ // in decreasing order 
    bool swapped; 
    for (int i = 0; i < arr_size-1; i++) { 
        swapped = false; 
        for (int j = 0; j < arr_size-i-1; j++) { 
            if (arr[j] < arr[j+1]) {  // for increasing order use '>' operator
                swap(&arr[j], &arr[j+1]); 
                swapped = true; 
            } 
        } 

        if (swapped == false) {
            break; 
        }
    } 
} 
  

int main(void) 
{
    int arr_1[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_1_size = sizeof(arr_1) / sizeof(arr_1[0]);

    bubbleSort(arr_1, arr_1_size);

    for (int i = 0; i < arr_1_size; i++) {
        std::cout << arr_1[i] << " "; 
    }
    std::cout << "\n";

    int arr_2[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_2_size = sizeof(arr_2) / sizeof(arr_2[0]);

    bubbleSort_optimalized(arr_2, arr_2_size);

    for (int i = 0; i < arr_2_size; i++) {
        std::cout << arr_2[i] << " "; 
    }

    return 0;
}