def encrypt(word):
    # function returns given string with characters pairs switched, for "house" result is "ohsue"
    # remember python do not support changing characters in string so we use list 
    word = list(word)
    for i in range(len(word)-1): # minus 1 is because of odd word size possibility
        word[i], word[i+1] = word[i+1], word[i]
    return "".join(word)


word = "programming"

word = encrypt(word)

print(word)