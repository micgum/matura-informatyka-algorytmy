"""
SELECTION SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions
            O(n) swaps 
        Best-case:
            O(n^2) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n) swaps
"""

def selectionSort(arr):
    # in increasing order
    min_element_index = None
    for i in range(len(arr)-1):
        min_element_index = i
        for j in range(i+1, len(arr)):
            if arr[j] < arr[min_element_index]:
                min_element_index = j
        arr[i], arr[min_element_index] = arr[min_element_index], arr[i] # swap two elements
    return arr
            

arr = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

arr = selectionSort(arr)

print(arr)