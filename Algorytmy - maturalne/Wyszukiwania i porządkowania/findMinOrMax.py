def findMin(arr):  # OR findMax
    # naive algorithm
    min = arr[0] # for max: max = arr[0]
    for i in arr:
        if i < min: # for max: arr[i] > max
            min = i
    return min # for max: return max


def findMinAndMax(arr):
    # optimal algorithm
    i = 2
    MAX = None
    MIN = None
    arr_size = len(arr)

    if arr_size < 2:
        MIN = MAX = arr[0]
    else:
        if arr[0] > arr[1]:
            MAX = arr[0]
            MIN = arr [1]
        else:
            MAX = arr[1]
            MIN = arr [0]

        while i+2 <= arr_size:
            if arr[i] > arr[i+1]:
                # arr[i] is potentially a MAX
                # arr[i+1] is potentially a MIN
                if arr[i] > MAX:
                    MAX = arr[i]
                if arr[i+1] < MIN:
                    MIN = arr[i+1]
            else:
                # arr[i+1] is potentially a MAX
                # arr[i] is potentially a MIN
                if arr[i+1] > MAX:
                    MAX = arr[i+1]
                if arr[i] < MIN:
                    MIN = arr[i]
            i += 2

        if arr_size % 2 == 1:
            if arr[i] > MAX:
                MAX = arr[i]
            if arr[i] < MIN:
                MIN = arr[i]

    return [MIN, MAX]


arr = [11, 2, 3, 42, 2, 133, 1, 344, 24, 2, 455, 22, 146, 5, 2434]

print(findMin(arr), end = ", ")
print(findMinAndMax(arr)[0])
print(findMinAndMax(arr)[1])
