#include <iostream>

void merge(int *arr, int left_index, int middle_index, int right_index)
{ // merges two subarrays of arr[]: first[0...middle_index], second[middle_index + 1, right_index]
    int left_subarray_size = middle_index - left_index + 1;
    int right_subarray_size = right_index - middle_index;

    int *left_subarray = new int[left_subarray_size];
    int *right_subarray = new int[right_subarray_size];

    for (int i = 0; i < left_subarray_size; i++) {
        left_subarray[i] = arr[left_index + i];
    }
    for (int i = 0; i < right_subarray_size; i++) {
        right_subarray[i] = arr[middle_index + 1 + i];
    }

    int i = 0;
    int j = 0;
    int k = left_index;

    while (i < left_subarray_size && j < right_subarray_size) {
        if (left_subarray[i] <= right_subarray[j]) {
            arr[k] = left_subarray[i];
            i++;
        } else {
            arr[k] = right_subarray[j];
            j++;
        }
        k++;
    }

    while (i < left_subarray_size) {
        arr[k] = left_subarray[i];
        i++;
        k++;
    }
    while (j < right_subarray_size) {
        arr[k] = right_subarray[j];
        j++;
        k++;
    }
}

/*
MERGE SORT:
    Complexity:
        Worst-case:
            O(n log m) 
        Best-case:
            O(n log n) or O(n)
        Average performance:
            O(n log n)
*/ 

void mergeSort(int *arr, int left_index, int right_index)
{ // sorting in increasing order (recursively) 
    if (left_index >= right_index) {
        return; // for recursion purpose
    }

    int middle_index = (left_index + right_index) / 2;

    mergeSort(arr, left_index, middle_index);
    mergeSort(arr, middle_index + 1, right_index);
    merge(arr, left_index, middle_index, right_index);
}

int main(void) 
{
    int arr[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    mergeSort(arr, 0, arr_size-1);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }

    return 0;
}