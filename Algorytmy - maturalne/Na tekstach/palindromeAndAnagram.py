def isPalindrome(string):
    # returns True if it is, False if it is not
    for i in range(0, int(len(string) / 2)): 
        if string[i] != string[len(string) - i - 1]:
            return False
    return True


def isPalinddrome_python_alternative(string):
    return string == string[::-1]


def isAnagram(str_1, str_2):
    str_1_size = len(str_1)
    str_2_size = len(str_2)

    if str_1_size != str_2_size:
        return False

    """
    YOU CAN USE THIS AS AN ALTERNATIVE:
        str_1 = sorted(str_1)
        str_2 = sorted(str_2)

        for i in range(str_1_size):
            if str_1[i] != str_2[i]:
                return False
        return True
    """

    count = [0] * 127

    # ord(x) converts character to ist's decimal ascii representation
    for i in str_1:
        count[ord(i)] += 1
    for i in str_2:
        count[ord(i)] -= 1

    for i in count:
        if i != 0:
            return False
    return True


str_1 = "kajak" # it is a palindrome
str_2 = "statek" # it is not a palindrome

print(isPalindrome(str_1), ",", isPalinddrome_python_alternative(str_2))
print(isAnagram(str_1, str_2), ",", isAnagram("adam", "dama"))