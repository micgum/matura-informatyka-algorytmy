#include <iostream>

std::string encrypt(std::string word)
{ // function returns given string with characters pairs switched, for "house" result is "ohsue"
    for (int i = 0; i < word.length()-1; i += 2) { // minus 1 is because of odd word size possibility
        char temp = word[i];
        word[i] = word[i+1];
        word[i+1] = temp;
    }
    return word;
}

int main(void)
{
    std::string word = "programming";

    word = encrypt(word);

    std::cout << word;

    return 0;
}