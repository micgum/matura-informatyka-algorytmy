#include <iostream>
#include <algorithm>

bool isPalindrome(std::string str) 
{ // returns true if it is, false if it is not
    for (int i = 0; i < (str.size() / 2); i++) {
        if (str[i] != str[str.size() - i - 1]) {
            return false;
        }
    }
    return true;
}

bool isAnagram(std::string str_1, std::string str_2) 
{ // returns true if it is, false if it is not
    size_t str_1_size = str_1.size();
    size_t str_2_size = str_2.size();

    if (str_1_size != str_2_size) {
        return false;
    }

    /*
    YOU CAN USE THIS AS AN ALTERNATIVE:
        std::sort(str_1.begin(), str_1.end());
        std::sort(str_2.begin(), str_2.end());

        for (int i = 0; i < str_1_size; i++) {
            if (str_1[i] != str_2[i]) {
                return false;
            }
        }
        return true;
    */

    int count[127] = {};

    for (int i = 0; i < str_1_size; i++) {
        count[str_1[i]]++;
    }
    for (int i = 0; i < str_2_size; i++) {
        count[str_2[i]]--;
    }

    for (int i = 0; i < 127; i++) {
        if (count[i] != 0) {
            return false;
        }
    }

    return true;
}

int main(void)
{
    std::string str_1 = "kajak"; // it is a palindrome
    std::string str_2 = "statek"; // it is not a palindrome

    std::cout << isPalindrome(str_1) << ", " << isPalindrome(str_2) << "\n"; 
    std::cout << isAnagram(str_1, str_2) << ", " << isAnagram("adam", "dama");

    return 0;
}