def triangleExists(a, b, c):
    greaterThanZeno = (a > 0 and b > 0 and c > 0)
    correctSides = (a+b > c and b+c > a and c+a > b)
    return greaterThanZeno and correctSides


print(triangleExists(1, 2, 4))
print(triangleExists(3, 4, 5))