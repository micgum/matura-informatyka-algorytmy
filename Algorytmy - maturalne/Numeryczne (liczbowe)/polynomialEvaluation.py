def hornerMethod_recursive(poly, n, x):
    # result of W(x)
    if n == 0:
        return poly[0]
    return x * hornerMethod_recursive(poly, n-1, x) + poly[n]


def hornerMethod_iterative(poly, n, x):
    # result of W(x)
    result = poly[0]

    for i in range(n):
        result = result * x + poly[i+1]
    return result


# W(x) = x^3 + 2x^2 - 5x + 4
poly = [1, 2, -5, 4]
n = len(poly)-1

# W(4)
print(hornerMethod_recursive(poly, n, 4))
# W(1)
print(hornerMethod_iterative(poly, n, 1))