#include <iostream>
#include <algorithm>

void swap(char *str_1, char *str_2) 
{
    char temp = *str_1;
    *str_1 = *str_2;
    *str_2 = temp;
}

std::string alphabeticalOrder(std::string str) 
{ // sorts given string in order from a to z
    // simple bubble sort, you can use any sorting algorithm
    for (int i = 0; i < str.size()-1; i++) {
        for (int j = 0; j < str.size()-i-1; j++) {
            if ((int)str[j] > (int)str[j+1]) {
                swap(&str[j], &str[j+1]);
            }
        }
    }
    return str;
}

int main(void)
{
    std::string str_1 = "bubble";
    std::string str_2 = "sort";

    str_1 = alphabeticalOrder(str_1);
    std::sort(str_2.begin(), str_2.end()); // with use of STL library

    std::cout << str_1 << "\n";
    std::cout << str_2;

    return 0;
}