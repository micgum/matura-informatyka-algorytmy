#include <iostream>
#include <iomanip>

double func(double x) 
{ // f(x) = 1 / (1+x^2)
    return 1/(1+x*x);
}


double trapezoidal(double left, double right, double n)
// function evalutes thr value of integral using Trapezoidal Rule
// left and right define range -> [left, right]; n is number of grid, higher value means more accuracy
{
    double grind_spacing = (right - left) / n;
    double sum = 0;

    for (int i = 1; i <= n; i++) {
        sum += 2 * func(left + i*grind_spacing);
    }

    return (grind_spacing / 2) * sum;
}   

double rectangular(double left, double right, double n) 
// function evalutes thr value of integral using Rectangular Method
// left and right define range -> [left, right]; n is number of grid, higher value means more accuracy
{
    double side = (right - left) / n;
    double sum = 0;
    double middle = left + (right - left)/(2.0 * n);

    for (int i = 0; i < n; i++) {
        sum += func(middle);
        middle += side;
    }

    return sum * side;
}

int main(void) 
{
    double left = -1;
    double right = 2;
    int n = 20;

    // trapezoidal method is little bit more accurate
    std::cout << std::fixed << std::setprecision(4) << trapezoidal(left, right, n) << "\n";
    std::cout << std::fixed << std::setprecision(4) << rectangular(left, right, n);

    return 0;
}

