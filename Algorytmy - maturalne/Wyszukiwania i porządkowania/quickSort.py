def partition(arr, left_index, right_index):
    # places smaller elements to the left of pivot and greater to the right
    pivot = arr[right_index]
    i = (left_index - 1)

    for j in range(left_index, right_index):
        if arr[j] < pivot:
            i += 1
            arr[i], arr[j] = arr[j], arr[i] # variables swap
    arr[i+1], arr[right_index] = arr[right_index], arr[i+1] # variables swap
    return (i + 1)


"""
QUICK SORT:
    Complexity:
        Worst-case:
            O(n^2)
        Best-case:
            O(n log n)
        Average performance:
            O(n log n)
"""

def quickSort(arr, left_index, right_index):
    # in increasing order
    if left_index < right_index:
        partitioning_index = partition(arr, left_index, right_index)

        quickSort(arr, left_index, partitioning_index - 1)
        quickSort(arr, partitioning_index + 1, right_index)


arr = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

quickSort(arr, 0, len(arr)-1)

print(arr)