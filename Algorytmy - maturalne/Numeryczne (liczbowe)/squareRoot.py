def squareRoot(num):
    # Newton-Raphson Method
    a_side = num
    b_side = 1
    precision = 0.00000001

    while a_side - b_side > precision:
        a_side = (a_side + b_side) / 2
        b_side = num / a_side
    return a_side

print(squareRoot(4), "\n", squareRoot(3.33))