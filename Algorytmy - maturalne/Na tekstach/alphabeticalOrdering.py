def swap(string, i, j):
    temp = list(string)
    temp[i], temp[j] = temp[j], temp[i]
    return "".join(temp)


def alphabeticalOrder(string):
    # sorts given string in order from a to z
    # simple bubble sort, you can use any sorting algorithm
    for i in range(len(string)-1):
        for j in range(len(string)-i-1):
            if ord(string[j]) > ord(string[j+1]):
                string = swap(string, j, j+1)
                #string[j] = string[j+1]
                # string[j+1] = temp
    return string


str_1 = "bubble"
str_2 = "sort"

str_1 = alphabeticalOrder(str_1)
str_2 = sorted(str_2) # with use of standart library, sorts string and returns list

print(str_1)
print("".join(str_2)) # joining list elements to one string