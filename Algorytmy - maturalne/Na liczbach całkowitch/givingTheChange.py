available_coins = [1, 2, 5, 10, 20, 50, 100, 500] 
coins_types_amount = len(available_coins)

def findMinimumChange(rest):
    result = []

    i = coins_types_amount - 1
    while i >= 0:
        while rest >= available_coins[i]:
            rest -= available_coins[i]
            result.append(available_coins[i])
        i -= 1

    for i in result:
        print(i, end = ", ")


findMinimumChange(93)