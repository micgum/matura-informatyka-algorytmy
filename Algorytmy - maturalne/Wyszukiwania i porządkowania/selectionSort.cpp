#include <iostream>

void swap(int *num_1, int *num_2) 
{
    int temp = *num_1;
    *num_1 = *num_2;
    *num_2 = temp;
}

/*
SELECTION SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions
            O(n) swaps 
        Best-case:
            O(n^2) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n) swaps
*/  

void selectionSort(int arr[], int arr_size)
{ // in increasing order
    int min_element_index;

    for (int i = 0; i < arr_size-1; i++) {
        min_element_index = i;
        for (int j = i+1; j < arr_size; j++) {
            if (arr[j] < arr[min_element_index]) {
                min_element_index = j;
            }
        }
        swap(&arr[i], &arr[min_element_index]);
    }
}

int main(void) 
{
    int arr[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    selectionSort(arr, arr_size);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }

    return 0;
}