def func(x):
    # f(x) = x^3 - 3x^2 + 2x - 6
    return x*(x*(x-3)+2)-6 #you can divide function into smaller factors using Horner's method


def chceckFunctionInterval(left, right, precision):
    if func(left) == 0:
        return left
    if func(right) == 0:
        return right

    while right-left > precision:
        middle = (left + right) / 2

        if func(middle) == 0.0:
            return middle
        
        if func(left) * func(middle) < 0: # f(x) == 0 in range [left, middle]
            right = middle
        else: # f(x) == 0 in range [middle, left]
            left = middle
    return (left + right) / 2


def chceckFunctionInterval_recursive(left, right, precision):
    if func(left) == 0:
        return left
    if func(right) == 0:
        return right

    middle = (left + right) / 2

    if right-left <= precision:
        return middle

    if func(left) * func(middle) < 0: # f(x) == 0 in range [left, middle]
        return chceckFunctionInterval_recursive(left, middle, precision)
    return chceckFunctionInterval_recursive(middle, right, precision) # f(x) == 0 in range [middle, left]


left = -10
right = 10
precision = 0.0000001

print(format(chceckFunctionInterval(left, right, precision), '7f')) # give 7 digits after floating point
print(format(chceckFunctionInterval_recursive(left, right, precision), '7f'))