#include <iostream>

int binarySearch(int arr[], int searched_element, int left_index, int right_index)
{ // searches for element index in arr[left_index...right_index]
    if (right_index <= left_index) {
        return (searched_element > arr[left_index]) ? (left_index + 1) : (left_index);
    }

    int middle_index = (left_index + right_index) / 2;

    if (searched_element == arr[middle_index]) {
        return middle_index;
    }
    if (searched_element > arr[middle_index]) {
        return binarySearch(arr, searched_element, middle_index+1, right_index);
    }
    return binarySearch(arr, searched_element, left_index, middle_index-1);
}

/*
BIANRY INSERTION SORT:
    Complexity:
        Complexity and swaps number is the same as Insertion Sort but number of
        comparasions is reduced due to use of binary search (from O(n) to O(log))
*/

void binaryInsertionSort(int *arr, int arr_size) 
{ // in increasing order
    int location;
    int key;
    int j;
    for (int i = 1; i < arr_size; i++) {
        key = arr[i];
        j = i-1;

        location = binarySearch(arr, key, 0, j);
        while (j >= location) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}

int main(void)
{
    int arr[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    binaryInsertionSort(arr, arr_size);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }

    return 0;
}