#include <iostream>
#include <cmath>

double squareRoot(double num)
{ // Newton-Raphson Method
    double a_side = 1;
    double b_side = num;
    constexpr double precision = 0.00000001;

    while (std::fabs(a_side - b_side) >= precision) {
        a_side = (a_side + b_side) / 2;
        b_side = num / a_side;
    }

    return a_side;
}

int main(void)
{
    std::cout << squareRoot(4) << "\n";
    std::cout << squareRoot(3.33);

    return 0;
}