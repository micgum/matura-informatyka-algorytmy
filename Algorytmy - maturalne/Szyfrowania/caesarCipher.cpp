#include <iostream>

std::string encrypt(std::string text, int key)
{
    std::string result = "";

    for (int i = 0; i < text.length(); i++) {
        if (isupper(text[i])) {
            result += char(int(text[i] + key - 65) % 26 + 65);
        } else {
            result += char(int(text[i] + key - 97) % 26 + 97);
        }
    }
    return result;
}

std::string decrypt(std::string text, int key)
{
    std::string result = "";

    for (int i = 0; i < text.length(); i++) {
        if (isupper(text[i])) {
            result += char(int(text[i] - key - 65) % 26 + 65);
        } else {
            result += char(int(text[i] - key - 97) % 26 + 97);
        }
    }
    return result;
}

int main(void)
{
    std::string text = "Caesar";
    int key = 4;

    std::cout << encrypt(text, key) << "\n";
    std::cout << decrypt(encrypt(text, key), key);

    return 0;
}