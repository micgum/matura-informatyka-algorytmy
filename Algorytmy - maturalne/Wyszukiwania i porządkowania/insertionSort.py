"""
INSERTION SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions 
            O(n^2) swaps 
        Best-case:
            O(n) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n^2) swaps
"""

def inserionSort(arr):
    # in increasing order
    for i in range(1, len(arr)):
        key = arr[i]
        j = i-1
        while j >= 0 and arr[j] > key:
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = key
    return arr


arr = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

arr = inserionSort(arr)

print(arr)