def encrypt(text, key):
    result = ""

    for char in text:
        if char.isupper():
            result += chr((ord(char) + key - 65) % 26 + 65)
        else:
            result += chr((ord(char) + key - 97) % 26 + 97)
    return result


def decrypt(text, key):
    result = ""

    for char in text:
        if char.isupper():
            result += chr((ord(char) - key - 65) % 26 + 65)
        else:
            result += chr((ord(char) - key - 97) % 26 + 97)
    return result


text = "Caesar"
key = 4

print(encrypt(text, key))
print(decrypt(encrypt(text, key), key))