#include <iostream>

void decToBase(long long number, long base) // max base is 36
{
    if (number > 0 ) {
        decToBase(number / base, base);
        if (number % base < 10) {
            std::cout << number % base;
        } else {
            auto temp = (char) 65 + (number % base) - 10;
            std::cout << temp;
        }
    }
}

int main(void) 
{
    decToBase(10, 2);

    return 0;
}