#include <iostream>

void swap(int *num_1, int *num_2)
{
    int temp = *num_1;
    *num_1 = *num_2;
    *num_2 = temp;
}

int partition(int *arr, int left_index, int right_index)
{ // places smaller elements to the left of pivot and greater to the right
    int pivot = arr[right_index];
    int i = (left_index - 1);

    for (int j = left_index; j <= right_index - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i+1], &arr[right_index]);
    return (i + 1);
}

/*
QUICK SORT:
    Complexity:
        Worst-case:
            O(n^2)
        Best-case:
            O(n log n)
        Average performance:
            O(n log n)
*/

void quickSort(int *arr, int left_index, int right_index) 
{ // in increasing order 
    if (left_index < right_index) {
        int partitioning_index = partition(arr, left_index, right_index);

        quickSort(arr, left_index, partitioning_index-1);
        quickSort(arr, partitioning_index+1, right_index);
    }
}

int main(void)
{
    int arr[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    quickSort(arr, 0, arr_size-1);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }

    return 0;
}