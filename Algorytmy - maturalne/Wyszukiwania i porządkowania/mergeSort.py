"""
MERGE SORT:
    Complexity:
        Worst-case:
            O(n log m) 
        Best-case:
            O(n log n) or O(n)
        Average performance:
            O(n log n)
"""

def mergeSort(arr):
    # sorting in increasing order (recursively) 
    if len(arr) > 1:
        middle_indxe = len(arr) // 2
        # merges two subarrays of arr[]: first[0...middle_index], second[middle_index + 1, right_index]
        left_subarray = arr[:middle_indxe]
        right_subarray = arr[middle_indxe:]

        mergeSort(left_subarray)
        mergeSort(right_subarray)

        i = j = k = 0

        while i < len(left_subarray) and j < len(right_subarray):
            if left_subarray[i] < right_subarray[j]:
                arr[k] = left_subarray[i]
                i += 1
            else:
                arr[k] = right_subarray[j]
                j += 1
            k += 1
        
        while i < len(left_subarray):
            arr[k] = left_subarray[i]
            i += 1
            k += 1
        while j < len(right_subarray):
            arr[k] = right_subarray[j]
            j += 1
            k += 1
    return arr


arr = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

arr = mergeSort(arr)

print(arr)