#include <iostream>

int hornerMethod_recursive(int poly[], int n, int x)
{ // result of W(x)
    if (n == 0) {
        return poly[0];
    }
    return x * hornerMethod_recursive(poly, n-1, x) + poly[n];
}

int hornerMethod_iterative(int poly[], int n, int x)
{ // result of W(x)
    int result = poly[0];

    for (int i = 1; i <= n; i++) {
        result = result * x + poly[i];
    }
    return result;
}

int main(void) 
{
    // W(x) = x^3 + 2x^2 - 5x + 4
    int poly[] {1, 2, -5, 4};
    int n = sizeof(poly) / sizeof(poly[0])-1;

    // W(4)
    std::cout << hornerMethod_recursive(poly, n, 4) << "\n";
    // W(1)
    std::cout << hornerMethod_iterative(poly, n, 1);

    return 0;
}