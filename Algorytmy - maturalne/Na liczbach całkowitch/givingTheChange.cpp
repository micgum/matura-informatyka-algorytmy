#include <iostream>
#include <vector>

constexpr int available_coins[] {1, 2, 5, 10, 20, 50, 100, 200, 500};
constexpr int coins_types_amount = sizeof(available_coins) / sizeof(available_coins[0]);

void findMinimumChange(long rest)
{ // greedy algorithm
    std::vector<int> result{};

    for (int i = coins_types_amount - 1; i >= 0; i--) { // search starts from last index (highest value coins)
        while (rest >= available_coins[i]) {
            rest -= available_coins[i];
            result.push_back(available_coins[i]);
        }
    }

    for (const int i : result) {
        std::cout << i << ", ";
    }
}

int main(void) 
{
    findMinimumChange(93);

    return 0;
}