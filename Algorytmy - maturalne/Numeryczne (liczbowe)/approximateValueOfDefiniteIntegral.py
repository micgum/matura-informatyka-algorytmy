def func(x):
    # f(x) = 1 / (1+x^2)
    return 1/(1+x*x)


def trapezoidal(left, right, n):
    # function evalutes thr value of integral using Trapezoidal Rule
    # left and right define range -> [left, right]; n is number of grid, higher value means more accuracy
    grid_spacing = (right - left) / n
    sum = 0.0

    for i in range(1, n+1):
        sum += 2 * func(left + i*grid_spacing)
    return (grid_spacing / 2) * sum


def rectangular(left, right, n):
    # function evalutes thr value of integral using Rectangular Method
    # left and right define range -> [left, right]; n is number of grid, higher value means more accuracy
    side = (right-left) / n
    sum = 0.0
    middle = left + (right - left)/(2.0 * n)

    for i in range(n):
        sum += func(middle)
        middle += side
    return sum * side


left = -1
right = 2
n = 20

# trapezoidal method is little bit more accurate
print(trapezoidal(left, right, n))
print(rectangular(left, right, n))