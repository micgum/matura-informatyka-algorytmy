"""
BUCKET SORT (for floating point values):
    Complexity:
        Worst-case:
            O(n^2)
        Best-case:
            O(n+k); k is komplexity of sorting individual buckets
        Average performance:
            O(n)
"""
def bucketSort(arr):
    # in increasing order 
    buckets = []

    for i in range(len(arr)):
        buckets.append([])
    
    # putting array elements into different buckets
    for i in arr:
        bucket_index = int(10 * i)
        buckets[bucket_index].append(i)

    # sorting every bucket
    for i in range(len(arr)):
        buckets[i] = sorted(buckets[i])

    # concatenetion of all buckets into arr[]
    index = 0
    for i in range(len(arr)):
        for j in range(len(buckets[i])):
            arr[index] = buckets[i][j]
            index += 1
    return arr


arr = [.132, .1323, .13, .134, .1, .34, .31, .1, .33, .3345, .642, .1, .4]

arr = bucketSort(arr)

print(arr)