#include <iostream>

struct Point2D
{
    double X;
    double Y;
};

double func(int x) 
{ // f(x) == 2x + 1
    return 2*x + 1;
}

bool isPointBelongsToSectionOfStraight(Point2D p, double a, double b) 
{ // a and b are straight limit, x belongs to <a; b>
    return (p.X >= a && p.X <= b) && func(p.X) == p.Y;
}

int main(void)
{
    Point2D p_1 {2, 5};
    Point2D p_2 {-1, 3};

    std::cout << isPointBelongsToSectionOfStraight(p_1, 2, 4) << "\n";
    std::cout << isPointBelongsToSectionOfStraight(p_2, 2, 4);

    return 0;
}
