def searchForPattern(word, pattern):
    if len(word) < len(pattern):
        return False
    if word == pattern:
        return True

    result = False

    for i in range(len(word)-len(pattern)):
        result = True
        for j in range(len(pattern)):
            if word[i+j] != pattern[j]:
                result = False
                break
        if result == True:
            return True
    return result


word = "lokomotywa"
pattern_1 = "motyw"
pattern_2 = "kot"

print(searchForPattern(word, pattern_1))
print(searchForPattern(word, pattern_2))