def quickExponentiation(base, power):
    # result is base^power
    if power == 0:
        return 1
    if power % 2 != 0:
        return base * quickExponentiation(base, power-1)
    
    result = quickExponentiation(base, power/2)
    return result * result


print(quickExponentiation(10, 2), ", ", quickExponentiation(2, 12))