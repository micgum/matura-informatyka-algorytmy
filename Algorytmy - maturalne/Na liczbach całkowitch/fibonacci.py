def fibonacci(n):
    temp_1 = 0
    temp_2 = 1

    for i in range(n-1):
        temp_2 += temp_1
        temp_1 = temp_2 - temp_1
    return temp_2


def fibonacci_not_optimalized(n):
    if n < 3:
        return 1
    return fibonacci_not_optimalized(n - 2) + fibonacci_not_optimalized(n - 1)


print(fibonacci(10))
print(fibonacci_not_optimalized(10))