from math import gcd # build in alternative for more info help(math)

def GCD(num_1, num_2):
    # recurrent
    if num_1 != num_2:
        return GCD((num_1 - num_2 if num_1 > num_2 else num_1), (num_2 - num_1 if num_2 > num_1 else num_2))
        """ OR
            temp_1 = (num_1 - num_2 if num_1 > num_2 else num_1) # or num_1 > num_2 and num_1 - num_2 or num_1
            temp_2 = (num_2 - num_1 if num_2 > num_1 else num_2)# or num_2 > num_1 and num_2 - num_1 or num_2

            return GCD(temp_1, temp_2);
        """
    return num_1


def GCD_optimalized(num_1, num_2):
    # recurrent
    if num_2 != 0:
        return GCD(num_2, num_1 % num_2)
    return num_1


print(GCD(68, 12))
print(GCD_optimalized(68, 12))
print(gcd(68, 12))
