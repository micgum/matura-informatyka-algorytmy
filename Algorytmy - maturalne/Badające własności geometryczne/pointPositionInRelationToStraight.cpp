#include <iostream>

struct Point2D
{
    double X;
    double Y;
};

double func(int x) 
{ // f(x) == 2x + 1
    return 2*x + 1;
}

bool isPointBelongsToStraight(Point2D p) 
{
    return func(p.X) == p.Y;
;}

int main(void)
{
    Point2D p_1 {2, 5};
    Point2D p_2 {-1, 3};

    std::cout << isPointBelongsToStraight(p_1) << "\n";
    std::cout << isPointBelongsToStraight(p_2);

    return 0;
}