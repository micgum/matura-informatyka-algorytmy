# MATURA Z INFORMATYKI:

## Algorytmy - wiedza ogólna:
+ Sortowania (bubble sort, quick sort, insertion sort)
+ Metoda równego podziału/połowienia (inaczej bisekcja)
+ Algorytmy typu dziel i zwyciężaj (divide and conquer) np. merge sort, quick sort, binary search
+ Algorytmy rekurencyjne np. silnia, ciąg Fibonacciego, Euklides (NWD - Najwięszy Wspólny Dzielnik)
+ Oblicznie przybliżonej złożoności obliczeniowej algorytmów

## Algorytmy - maturalne:
+ ### Algorytmy na liczbach całkowitych:
  + reprezentacja liczb w dowolnym systemie pozycyjnyy (liczbowym) np. dwójkowym, szesnastkowym
  + sprawdzanie czy liczba jest liczbą pierwszą (sito Eratostenesa)
  + sprawdzanie czy liczba jest liczbą doskonałą
  + rozkładanie liczby na czynniki pierwsze
  + iteracyjny i rekurencyjny algorytm Euklidesa (NWD)
  + iteracyjne i rekurencyjne obliczanie n-tego wyrazu ciągu Fibonacciego
  + wydawanie reszty (metoda zachłanna)
+ ### Algorytmy wyszukiwania i porządkowania (sortowania):
  + znajdowanie najmniejszego i największego elementu w zbiorze danych (algorytm naiwny i optymalny)
  + sortowanie rosnąco lub malejąco zestawu danych (liczb): bąbelkowe, przez selekcję/wybór, przez wsatwianie liniowe/binarne, przez scalanie, szybkie, kubełkowe
+ ### Algorytmy numeryczne (liczbowe):
  + obliczanie pierwiastka kwadratowego liczby
  + oblicznie wartości wielomianu (schemat Hornera)
  + zastosowanie schematu Homera: reprezentacja liczb w róznych systemach liczbowych, szybkie podnoszenie do potęgi
  + wyznaczanie miejsc zerowych funkcji (metoda połowienia - bisekcja)
  + oblicznie pola obszarów zamkniętych (metoda trapezów i prostokątów)
+ ### Algorytmy na tekstach (zmeinnych typu string lub char*):
  + sprawdzanie czy ciąg znaków jest palindormem lub anagramem
  + porządkowanie ciągu znaków alfabetycznie
  + wyszukiwanie wzorca w tekście
+ ### Algorytmy szyfrowania:
  + szyfr Cezara
  + szyfr przestawniowy
+ ### Akgorytmy badające własności geometryczne:
  + sprawdzanie warunku trójkąta
  + badanie położenia punktu względem prostej
  + badanie przynależności punktu do odcinka
  + przecinanie się odcinków
  + przynależność punktu do obszaru
  + konstrukjce rekurencyjne: drzewo binarne

## Struktury danych:
### Dla C++ dostępne są w STL - Standart Template Library; Dla Pythona dostępna jest list() ale może przyjmować postać innych struktur oraz set() i dictionary()
+ Tablica (jendno i wielowymiarowa)
+ Stos
+ Kolejka (jednokierunkowa, dwukierunkowa, pryiorytetowa)
+ Vektor (implementacja podobna do tablicy - tylko dla C++ z użyciem STL)
+ Mapa (tablica asocjacyjna bez powtarzających się kluczy)
+ Set (zbiór uporządkowany, elementy się nie powtarzają)
+ Lista (jednokierunkowa lub dwókierunkowa)

## Obsługa plików:
+ Dla C++ z użyciem biblioteki fstream (file stream) i obiektu o tej samej nazwie (std::fstream obiekt("ścieżka do pliku"))
+ Dla Pythona z użyciem open("ścieżka do pliku") (np with open(...) as plik lub plik = open(...))

## Arkusz kalkulacyjny:
+ podstawowe operacje logincze i matematyczne
+ tworzenie wykresów (reprezentacji graficznej wyniku zadań)

## Bazy danych:
+ zapytania do bazy za pomocą: Kwerend MS Access lub zapytań języka SQL

## Inne:
+ Podstawy wiedzy o grafice (formaty plików, grafika wektorowa, rastowa, palety barw: CMYK, RGB)
+ Podstawy wiedzy o technologiach webowych (PHP, Java Script, certyfikat SSL, metody HTTP i HTTPS, łączenie klient-serwer)
+ Podstawy wiedzy o sieciach komputerowych (wystarczy wiedza z kanału Pasja Informatyki i wiedza o adresach IP w sieciach)
