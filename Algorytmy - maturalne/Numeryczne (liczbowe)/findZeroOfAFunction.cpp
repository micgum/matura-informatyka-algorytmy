#include <iostream>
#include <iomanip>

double func(double x)
{ 
    // f(x) = x^3 - 3x^2 + 2x - 6
    return x*(x*(x-3)+2)-6; // you can divide function into smaller factors using Horner's method
}

double chceckFunctionInterval(double left, double right, double precision)
{ // left and right are limits of the function range -> [left, right]
    if (func(left) == 0.0) {
        return left;
    }
    if (func(right) == 0.0) {
        return right;
    }

    while (right-left > precision) {
        double middle = (left + right) / 2;

        if (func(middle) == 0.0) {
            return middle;
        }
        if (func(left) * func(middle) < 0) { // f(x) == 0 in range [left, middle]
            right = middle;
        } else { // f(x) == 0 in range [middle, left]
            left = middle;
        }
    }
    return (left + right) / 2;
}

double chceckFunctionInterval_recursive(double left, double right, double precision)
{ // left and right are limits of the function range -> [left, right]
    if (func(left) == 0.0) {
        return left;
    }
    if (func(right) == 0.0) {
        return right;
    }

    double middle = (left + right) / 2;

    if (right-left <= precision) {
        return middle;
    }

    if (func(left) * func(middle) < 0) { // f(x) == 0 in range [left, middle]
        return chceckFunctionInterval_recursive(left, middle, precision);
    }
    return chceckFunctionInterval_recursive(middle, right, precision); // f(x) == 0 in range [middle, left]
}

int main(void) 
{
    constexpr int precision = 0.0000001;
    double left = -10, right = 10;

    std::cout << std::fixed << std::setprecision(7) << chceckFunctionInterval(left, right, precision) << "\n";
    std::cout << std::fixed << std::setprecision(7) << chceckFunctionInterval_recursive(left, right, precision);

    return 0;
}