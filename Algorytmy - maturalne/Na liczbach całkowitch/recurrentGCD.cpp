#include <iostream>

long GCD(long long num_1, long long num_2) 
{ // recurrent
    if (num_1 != num_2) {
        return GCD(((num_1 > num_2) ? (num_1 - num_2) : num_1), ((num_2 > num_1) ? (num_2 - num_1) : num_2));
        /* OR
            long long temp_1 = ((num_1 > num_2) ? (num_1 - num_2) : num_1);
            long long temp_2 = ((num_2 > num_1) ? (num_2 - num_1) : num_2);

            return GCD(temp_1, temp_2);
        */
    }
    return num_1;
}

long GCD_optimalized(long long num_1, long long num_2)
{ // recurrent
    if (num_2 != 0) {
        return GCD(num_2, num_1 % num_2);
    }
    return num_1;
}

int main(void) 
{
    std::cout << GCD(68, 12) << "\n";
    std::cout << GCD_optimalized(68, 12);

    return 0;
}