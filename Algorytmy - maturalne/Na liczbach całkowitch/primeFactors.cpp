#include <iostream>
#include <cmath>

void primeFactors(long long number) 
{
    int prime_num = std::sqrt(number);
    int i = 2;

    while (number > 1 && i <= prime_num) {
        while (number % i == 0) {
            std::cout << i << " ";
            number /= i;
        }
        i++;
    }

    if (number > 1) {
        std::cout << number << "\n";
    }
}

int main(void) 
{
    primeFactors(15);

    return 0;
}