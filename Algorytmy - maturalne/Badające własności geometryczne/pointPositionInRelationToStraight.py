class Point2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def func(x):
    # f(x) = 2x + 1
    return 2*x + 1


def isPointBelongsToStraight(point: Point2D):
    return func(point.x) == point.y


p_1 = Point2D(2, 5)
p_2 = Point2D(-1, 3)

print(isPointBelongsToStraight(p_1))
print(isPointBelongsToStraight(p_2))