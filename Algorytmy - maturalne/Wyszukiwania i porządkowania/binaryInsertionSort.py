def binarySearch(arr, searched_element, left_index, right_index):
    # searches for element index in arr[left_index...right_index]
    if right_index <= left_index:
        return left_index+1 if searched_element > arr[left_index] else left_index

    middle_index = (left_index + right_index) // 2

    if searched_element == arr[middle_index]:
        return middle_index
    if searched_element > arr[middle_index]:
        return binarySearch(arr, searched_element, middle_index+1, right_index)
    return binarySearch(arr, searched_element, left_index, middle_index-1)


"""
BIANRY INSERTION SORT:
    Complexity:
        Complexity and swaps number is the same as Insertion Sort but number of
        comparasions is reduced due to use of binary search (from O(n) to O(log))
"""

def binaryInsertionSort(arr):
    # in increasing order
    for i in range(1, len(arr)):
        key = arr[i]
        j = binarySearch(arr, key, 0, i-1)
        arr = arr[:j] + [key] + arr[j:i] + arr[i+1:]
    return arr

arr = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

arr = binaryInsertionSort(arr)

print(arr)