#include <iostream>

struct Pair
{
    int MAX;
    int MIN;
};

int findMin(int arr[], int arr_size)  // OR findMax
{ // naive algorithm
    int min = arr[0]; // for max: int max = arr[0]

    for (int i = 0; i < arr_size; i++) {
        if (arr[i] < min) { // for max: arr[i] > max
            min = arr[i];
        }
    }
    return min; // for max: return max
}

Pair findMinAndMax(int arr[], int arr_size) 
{ // optimal algorithm
    int i = 2;
    int MIN, MAX;
    if (arr_size < 2) {
        MIN = MAX = arr[0];
    } else {
        if (arr[0] > arr[1]) {
            MAX = arr[0];
            MIN = arr[1];
        } else {
            MAX = arr[1];
            MIN = arr[0];
        }

        while (i+2 <= arr_size) {
            if (arr[i] > arr[i+1]) {
                // arr[i] is potentially a MAX
                // arr[i+1] is potentially a MIN
                if (arr[i] > MAX) {
                    MAX = arr[i];
                }
                if (arr[i+1] < MIN) {
                    MIN = arr[i+1];
                }
            } else {
                // arr[i+1] is potentially a MAX
                // arr[i] is potentially a MIN
                if (arr[i+1] > MAX) {
                    MAX = arr[i+1];
                }
                if (arr[i] < MIN) {
                    MIN = arr[i];
                }
            }
            i += 2;
        }

        if (arr_size % 2 == 1) {
            if (arr[i] > MAX) {
                MAX = arr[i];
            }
			if (arr[i] < MIN) {
                MIN = arr[i];
            }
        }
    }

    Pair result;
    result.MAX = MAX;
    result.MIN = MIN;

    return result;
}

int main(void)
{
    int arr[] {11, 2, 3, 42, 2, 133, 1, 344, 24, 2, 455, 22, 146, 5, 2434};
    int arr_size = sizeof(arr) / sizeof(*arr);

    int min = findMin(arr, arr_size);
    Pair result = findMinAndMax(arr, arr_size);

    std::cout << min << " " << result.MIN << "\n";
    std::cout << result.MAX;

    return 0;
}