#include <iostream>

bool triangleExists(int a, int b, int c)
{
    bool greaterThanZero = (a > 0 && b > 0 && c > 0);
    bool correctSides = (a+b > c && b+c > a && c+a > b);
    return greaterThanZero && correctSides;
}

int main(void)
{
    std::cout << triangleExists(1, 2, 4) << "\n";
    std::cout << triangleExists(3, 4, 5);

    return 0;
}