"""
BUBBLE SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions
            O(n^2) swaps 
        Best-case:
            O(n) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n^2) swaps
"""

def bubbleSort(arr):
    # in increasing order
    for i in range(len(arr)-1):
        for j in range(len(arr)-i-1):
            if arr[j] > arr[j+1]: # for decreasing order use '<' operator
                temp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = temp
    return arr


def bubbleSort_optimalized(arr):
    # in decreasing order
    swapped = None
    for i in range(len(arr)-1):
        swapped = False
        for j in range(len(arr)-i-1):
            if arr[j] < arr[j+1]: # for increasing order use '>' operator
                temp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = temp
                swapped = True
        if swapped == False:
            break
    return arr


arr_1 = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]
arr_2 = [132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4]

arr_1 = bubbleSort(arr_1)
arr_2 = bubbleSort_optimalized(arr_2)

print(arr_1)
print(arr_2)