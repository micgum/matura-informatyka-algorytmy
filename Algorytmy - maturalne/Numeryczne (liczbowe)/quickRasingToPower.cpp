#include <iostream>

long long quickExponentiation(long long base, unsigned int power)
{ // result is base^power
    if (power == 0) {
        return 1;
    } 
    if (power % 2 != 0) {
        return base * quickExponentiation(base, power-1);
    }

    long long result = quickExponentiation(base, power/2);
    return result * result;
}

int main(void) 
{
    std::cout << quickExponentiation(10, 2) << ", " << quickExponentiation(2, 12);

    return 0;
}