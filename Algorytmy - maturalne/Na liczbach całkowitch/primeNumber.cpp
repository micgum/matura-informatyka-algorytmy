#include <iostream>

bool isNumberPrime(long long number)
{
    if (number < 2) {
        return false;
    }
    for (int i = 2; (i * i) <= number; i++) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

int main(void) 
{
    std::cout << isNumberPrime(9);

    return 0;
}