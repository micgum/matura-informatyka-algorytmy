#include <iostream>

long  GCD(long long num_1, long long num_2) 
{ // iterative
    while (num_1 != num_2) {
        if (num_1 > num_2) {
            num_1 -= num_2; // or num_1 = num_1 - num_2
        } else {
            num_2 -= num_1; // or num_2 = num_2 - num_1
        }
    }
    return num_1; // or num_2, both variables store GCD(num_1, num_2) result
}

long GCD_optimalized(long long num_1, long long num_2) 
{ // iterative
    long long temp;

    while (num_2 != 0) {
        temp = num_2;
        num_2 = num_1 % num_2;
        num_1 = temp;
    }
    return num_1;
}

int main(void) 
{
    std::cout << GCD(68, 12) << "\n";
    std::cout << GCD_optimalized(68, 12);

    return 0;
}