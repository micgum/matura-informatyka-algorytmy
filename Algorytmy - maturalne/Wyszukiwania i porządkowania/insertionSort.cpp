#include <iostream>

/*
INSERTION SORT:
    Complexity:
        Worst-case:
            O(n^2) comparasions 
            O(n^2) swaps 
        Best-case:
            O(n) comparasions
            O(1) swaps
        Average performance:
            O(n^2) comparasions
            O(n^2) swaps
*/ 

void insertionSort(int *arr, int arr_size) 
{ // in increasing order
    int key;
    int j;
    for (int i = 1; i < arr_size; i++) {
        key = arr[i];
        j = i-1;

        while (j >= 0 && arr[j] > key) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}

int main(void)
{
    int arr[] {132, 1323, 13, 134, 1, 34, -31, -1, 33, 3345, 642, 1, -4};
    int arr_size = sizeof(arr) / sizeof(arr[0]);

    insertionSort(arr, arr_size);

    for (int i = 0; i < arr_size; i++) {
        std::cout << arr[i] << " ";
    }

    return 0;
}