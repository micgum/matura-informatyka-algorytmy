from math import gcd # build in alternative for more info help(math)

def GCD(num_1, num_2):
    # iterative
    while num_1 != num_2:
        if num_1 > num_2:
            num_1 -= num_2 # or num_1 = num_1 - num_2
        else :
            num_2 -= num_1 # or num_2 = num_2 - num_1
    return num_1 # or num_2, both variables store GCD(num_1, num_2) result


def GCD_optimalized(num_1, num_2):
    # iterative
    temp = 0
    while num_2 != 0:
        temp = num_2
        num_2 = num_1 % num_2
        num_1 = temp
    return num_1


print(GCD(68, 12))
print(GCD_optimalized(68, 12))
print(gcd(68, 12))