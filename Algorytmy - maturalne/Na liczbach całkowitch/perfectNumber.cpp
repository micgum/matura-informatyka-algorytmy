#include <iostream>

bool isNumberPerfect(long long number)
{
    int i = 1, sum = 0;
    while (i < number) {
        if (number % i == 0) {
            sum += i;
        }
        i++;
    }

    if (sum == number) {
        return true;
    }
    return false;
}

int main(void) 
{
    std::cout << isNumberPerfect(8128);

    return 0;
}