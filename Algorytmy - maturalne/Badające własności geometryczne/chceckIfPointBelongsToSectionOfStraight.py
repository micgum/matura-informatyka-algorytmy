class Point2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def func(x):
    # f(x) = 2x + 1
    return 2*x + 1


def isPointBelongsToStraight(point: Point2D, a, b):
    # a and b are straight limit, x belongs to <a; b>
    return (point.x >= a and point.x <= b) and func(point.x) == point.y


p_1 = Point2D(2, 5)
p_2 = Point2D(-1, 3)

print(isPointBelongsToStraight(p_1, 2, 4))
print(isPointBelongsToStraight(p_2, 2, 4))